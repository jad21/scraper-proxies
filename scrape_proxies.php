<?php

class http
{
    public static $headers = [
        "Connection: keep-alive",
        "Accept: */*",
        "Pragma: no-cache",
        "Cache-Control: no-cache",
        "User-Agent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36'",
        "Referer: {page}",
        "Accept-Encoding: gzip, deflate, sdch, br",
        "Accept-Language: es-ES,es;q=0.8,en;q=0.6",
    ];

    public static function get($url, $headers=array())
    {
        $file_url = "data/requests/".md5($url);
        if (file_exists($file_url)) {
            return file_get_contents($file_url);
        }
        exit();
        $headers = array_merge(static::$headers, $headers);
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 15,
            CURLOPT_HTTPHEADER => $headers
            )
        );
        $data = curl_exec($ch);
        curl_close($ch);

        file_put_contents($file_url, $data);
        return $data;
    }
}

class ScrapeProxydb
{
    public $page_start = "http://proxydb.net/?protocol=http&protocol=https&availability=30&response_time=10&offset=%s";
    public function handle()
    {
        $offset = 0;
        $num_row = 10;
        $found_total = 0;
        $headers = [];
        $headers["Host"] = "http://proxydb.net";
        while (TRUE){
            $ips = [];
            $uri = sprintf($this->page_start, $offset);
            $content_page = http::get($uri);
            try {
                $xml = new SimpleXMLElement($content_page);
            } catch (Exception $e) {
                
            }
            print_r($xml);
            break;
        }
    }
}

$obj = new ScrapeProxydb();
$obj->handle();
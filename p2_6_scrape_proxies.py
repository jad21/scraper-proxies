# -*- coding: utf-8 -*-
import os, sys;
BASE = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(BASE, "site-packages"))

import xml.etree.ElementTree as et
from bs4 import BeautifulSoup
# from lxml import html
import requests
import os
import re
import hashlib
import traceback


__author__ = '''
    user:     jad21 <Jose Angel>
    email:    esojangel@gmail.com
    whatsapp: +584120309959
'''
req = requests.Session()
req.headers = {
    # "Host": "",
    "Connection": "keep-alive",
    "Accept": "*/*",
    "Pragma": "no-cache",
    "Cache-Control": "no-cache",
    # "X-Requested-With": "XMLHttpRequest",
    "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
    "Referer": "{page}",
    "Accept-Encoding": "gzip, deflate, sdch, br",
    "Accept-Language": "es-ES,es;q=0.8,en;q=0.6",
    # "Upgrade-Insecure-Requests": "1"
}
CACHEABLE = False


class http(object):
    """docstring for http"""

    @staticmethod
    def _b_url(url):
        return hashlib.md5(url.encode('utf-8')).hexdigest()

    @staticmethod
    def get(url, *p, **kp):
        if CACHEABLE:
            if not os.path.exists('data/requests'):
                os.makedirs(os.path.join(*'data/requests'.split("/")))
            url_b = os.path.join("data", "requests", http._b_url(url))
            print(url_b)
            if os.path.exists(url_b):
                with open(url_b, "rb") as f:
                    return f.read()
        # exit()
        req.headers["Referer"] = url
        res = req.get(url, *p, **kp)

        if res.status_code != 200:
            res.raise_for_status()

        if CACHEABLE and res.status_code == 200:
            with open(url_b, "wb") as f:
                f.write(res.content)
        return res.content


class Data():
    file = os.path.join(BASE, "proxies.csv")
    # file_checked = os.path.join(BASE, "proxies_checked.csv")
    file_checked = "/home/soundclo/www/IP/proxy.csv"

    def all(self, **kw):
        ips = set()
        if os.path.exists(kw.get("file", self.file)):
            with open(kw.get("file", self.file), "r") as f:
                for line in f.readlines():
                    ips.add(line.replace("\n", ""))
        return list(ips)

    def insert_many(self, *proxies, **kw):
        ips = self.all(**kw)
        with open(kw.get("file", self.file), "w+") as f:
            f.seek(0)
            proxies = list(proxies) + ips
            proxies = set(proxies)
            content = "\n".join([p.strip() for p in list(proxies)])
            f.write(content + "\n")
            f.truncate()

    def delete(self, *ip, **kw):
        if ip:
            found = False
            items = self.all(**kw)
            for p in ip:
                if p in items:
                    found = True
            if found:
                with open(kw.get("file", self.file), "r+") as f:
                    d = f.readlines()
                    f.seek(0)
                    ips = "\n".join(ip)
                    content = "\n".join([p.strip() for p in d if p.strip() not in ips])
                    f.write(content + "\n")
                    f.truncate()


class ScrapeProxydb(object):
    """docstring for ScrapeProxydb"""
    page_start = "http://proxydb.net/?protocol=http&protocol=https&availability=30&response_time=10&offset=%s"

    def handle(self):
        offset = 0
        num_row = 10
        found_total = 0
        while(True):
            ips = set()
            req.headers["Host"] = "http://proxydb.net"
            uri = self.page_start % (offset)
            content_page = http.get(uri)
            page = BeautifulSoup(content_page, "html.parser")
            found_total_node = page.find_all('small', attrs={"class": "text-muted"})
            # found_total_node = page.xpath("//*[contains(text(), 'Found')]")
            if found_total_node:
                found_total = found_total_node[0]
                found_total = int(re.sub(r"\D+", "", found_total.text, 0))
                tr_table = page.find("tbody").find_all("tr")
                for tr in tr_table:
                    ips.add(tr.find("td").a.text.strip())

                Data().insert_many(*ips)

            if offset > found_total:
                print(found_total)
                print("fin")
                break
            else:
                offset += num_row
                print("offset", offset)


class ScrapeNntime(object):
    page_start = "http://nntime.com/proxy-list-%02d.htm"

    def get_between(self, content, start, end):
        r = content.split(start)
        if len(r) > 1:
            return r[1].split(end)[0]
        return ""

    def handle(self):
        num_page = 0
        # print("init")
        final_page = None
        while True:
            proxies = set()
            num_page += 1
            if final_page and num_page >= final_page:
                print("End page")
                break
            uri = self.page_start % (num_page)
            content_page = http.get(uri)
            page = BeautifulSoup(content_page, "html.parser")
            final_page = page.find('div', attrs={"id": "navigation"}).find_all("a")[-2].text
            final_page = int(final_page) if final_page else None
            print("page:", num_page, "to:", final_page)
            var_ports = page.find('html').find("head").find_all("script")[-1].text.strip()
            tr_table = page.find('table', attrs={"id": "proxylist"}).find_all("tr")
            if var_ports:
                var_ports_values = {}
                list_var_port = var_ports.split(";")
                list_var_port.pop()
                for var in list_var_port:
                    var = var.split("=")
                    var_ports_values[var[0].encode()] = var[1].encode()

                # print(tr_table)
                for tr in tr_table[0:]:
                    td = tr.find_all("td")
                    if len(td) <= 1:
                        continue
                    text_ip = td[1].text.split("document")[0]
                    encryt_port = self.get_between(td[1].script.text, 'document.write(":"', ")").replace("+", "")

                    if encryt_port:
                        for k, v in var_ports_values.items():
                            encryt_port = encryt_port.replace(k, v)
                        proxies.add("%s:%s" % (text_ip.strip(), encryt_port.strip()))

                Data().insert_many(*proxies)
            if final_page and num_page >= final_page:
                print("End page")
                break
            # break
        print("end")


class ScrapeIpAdress(object):
    page_start = "http://www.ip-adress.com/proxy_list/?k=time&d=desc"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        proxies = set()
        re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
        Data().insert_many(*proxies)


class ScrapeProxyDaily(object):
    page_start = "http://proxy-daily.com/"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = BeautifulSoup(content_page, "html.parser")
        pages_href = [a["href"] for a in page_main.find_all("a", attrs={"class": "more-link"})]
        for uri_page in pages_href:
            content_page = http.get(uri_page)
            if "You Are Banned." in content_page.decode():
                continue
            proxies = set()
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            Data().insert_many(*proxies)


class ScrapeProxyServerList(object):
    page_start = "http://proxyserverlist-24.blogspot.com.au"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = BeautifulSoup(content_page, "html5lib")
        pages_href = [a["href"] for a in page_main.select("div.jump-link a")]
        for uri_page in pages_href:
            content_page = http.get(uri_page)
            proxies = set()
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            Data().insert_many(*proxies)
            # break


class ScrapeNewFreshProxies(object):
    page_start = "http://newfreshproxies-24.blogspot.com.au/"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = BeautifulSoup(content_page, "html5lib")
        pages_href = [a["href"] for a in page_main.select("div.jump-link a")]

        for uri_page in pages_href:
            content_page = http.get(uri_page)
            proxies = set()
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            # print(proxies)
            Data().insert_many(*proxies)
            # break


class ScrapeLiveSocksNet(object):
    page_start = "http://www.live-socks.net"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = BeautifulSoup(content_page, "html5lib")
        pages_href = [a["href"] for a in page_main.select("div.jump-link a")]

        for uri_page in pages_href:
            content_page = http.get(uri_page)
            proxies = set()
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            # print(proxies)
            Data().insert_many(*proxies)
            # break


class ScrapeVipSocks24(object):
    page_start = "http://www.vipsocks24.net/"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = BeautifulSoup(content_page, "html5lib")
        pages_href = [a["href"] for a in page_main.select("div.jump-link a")]

        for uri_page in pages_href:
            content_page = http.get(uri_page)
            proxies = set()
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            # print(proxies)
            Data().insert_many(*proxies)
            # break


class ScrapeFreshProxiesOrg(object):
    page_start = "http://www.freshproxy.org/"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        proxies = set()
        re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
        Data().insert_many(*proxies)


if __name__ == '__main__':
    print("begin app.")
    klass = [ScrapeProxydb, ScrapeNntime, ScrapeIpAdress, ScrapeProxyDaily, ScrapeProxyServerList, ScrapeNewFreshProxies, ScrapeLiveSocksNet, ScrapeVipSocks24, ScrapeFreshProxiesOrg]
    for c in klass:
        try:
            print "page", c.page_start
            if "Host" in req.headers:
                del req.headers["Host"]
            c().handle()
        except Exception as e:
            print("Error")
            print traceback.format_exc()
    print("end app.")

# -*- coding: utf-8 -*-
import os, sys;
BASE = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(BASE, "site-packages"))

from requests.packages import urllib3
from p2_6_scrape_proxies import Data
import traceback


urllib3.disable_warnings()
__author__ = '''
    user: jad21 <Jose Angel>
    email: esojangel@gmail.com
    whatsapp: +584120309959
'''
DEBUG = True
model = Data()


def verify_ip(ip):
    try:
        print("verify %s..." % (ip))
        proxy = urllib3.ProxyManager('http://%s' % (ip))
        res = proxy.request('GET', 'http://httpbin.org/ip', timeout=6)
        if res.status == 200:
            print(res.data)
            model.insert_many(ip, file=model.file_checked)
            return
        else:
            raise Exception("bad response")
    except:
        model.delete(ip)
        model.delete(ip, file=model.file_checked)
        print "Error conn %s" % (ip)
        if DEBUG:
            print traceback.format_exc()


def main():
    items = Data().all()
    for ip in items:
        verify_ip(ip)


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-

from lxml import html
import requests
import os
import sys
import re
import urllib
import glob
import json
import time
import hashlib


__author__ = '''
    user:     jad21 <Jose Angel>
    email:    esojangel@gmail.com
    whatsapp: +584120309959
'''
req = requests.Session()
req.headers = {
    # "Host": "",
    "Connection": "keep-alive",
    "Accept": "*/*",
    "Pragma": "no-cache",
    "Cache-Control": "no-cache",
    # "X-Requested-With": "XMLHttpRequest",
    "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
    "Referer": "{page}",
    "Accept-Encoding": "gzip, deflate, sdch, br",
    "Accept-Language": "es-ES,es;q=0.8,en;q=0.6",
    # "Upgrade-Insecure-Requests": "1"
}
CACHEABLE = False


class http(object):
    """docstring for http"""

    @staticmethod
    def _b_url(url):
        return hashlib.md5(url.encode('utf-8')).hexdigest()

    @staticmethod
    def get(url, *p, **kp):
        if CACHEABLE:
            if not os.path.exists('data/requests'):
                os.makedirs(os.path.join(*'data/requests'.split("/")))
            url_b = os.path.join("data", "requests", http._b_url(url))
            # print(url_b)
            if os.path.exists(url_b):
                with open(url_b, "rb") as f:
                    return f.read()
        # exit()
        req.headers["Referer"] = url
        res = req.get(url, *p, **kp)

        if res.status_code != 200:
            res.raise_for_status()

        if CACHEABLE and res.status_code == 200:
            with open(url_b, "wb") as f:
                f.write(res.content)
        return res.content


class Data():
    file = "proxies.csv"

    def all(self):
        ips = set()
        if os.path.exists(self.file):
            with open(self.file, "r") as f:
                for line in f.readlines():
                    ips.add(line.replace("\n", ""))
        return list(ips)

    def insert_many(self, *proxies):
        ips = self.all()
        with open(self.file, "w") as f:
            f.seek(0)
            proxies = list(proxies) + ips
            proxies = set(proxies)
            content = "\n".join([p.strip() for p in list(proxies)])
            f.write(content + "\n")
            f.truncate()

    def delete(self, *ip):
        with open(self.file, "r+") as f:
            d = f.readlines()
            f.seek(0)
            ips = "\n".join(ip)
            content = "\n".join([p.strip() for p in d if p.strip() not in ips])
            f.write(content + "\n")
            f.truncate()


class ScrapeProxydb(object):
    """docstring for ScrapeProxydb"""
    page_start = "http://proxydb.net/?protocol=http&protocol=https&availability=30&response_time=10&offset={offset}"

    def handle(self):
        offset = 0
        num_row = 10
        found_total = 0
        while(True):
            ips = set()
            req.headers["Host"] = "http://proxydb.net"
            uri = self.page_start.format(offset=str(offset))
            content_page = http.get(uri)
            page = html.fromstring(content_page)
            found_total_node = page.xpath("//*[contains(text(), 'Found')]")
            if found_total_node:
                found_total = found_total_node[0]
                found_total = int(re.sub(r"\D+", "", found_total.text, 0, re.IGNORECASE))

                tr_table = page.xpath("//tbody/tr")
                num_row = len(tr_table)
                for tr in tr_table:
                    ip = tr.xpath("td[1]/a/text()")
                    protocol = tr.xpath("td[2]/text()")
                    if ip:
                        # ips.add("{}://{}".format(protocol[0].strip().lower(), ip[0].strip()))
                        ips.add(ip[0].strip())

                Data().insert_many(*ips)

            if offset > found_total:
                print(found_total)
                print("fin")
                break
            else:
                offset += num_row
                print("offset", offset)
            # break


class ScrapeNntime(object):
    page_start = "http://nntime.com/proxy-list-%02d.htm"

    def get_between(self, content, start, end):
        r = content.split(start)
        if len(r) > 1:
            return r[1].split(end)[0]
        return ""

    def handle(self):
        num_page = 0
        print("init")
        final_page = None
        while True:
            proxies = set()
            num_page += 1
            if final_page and num_page >= final_page:
                print("End page")
                break
            uri = self.page_start % (num_page)
            content_page = http.get(uri)
            page = html.fromstring(content_page)
            final_page = page.xpath('//*[@id="navigation"]/a[last()-1]/text()')
            final_page = int(final_page[0]) if final_page else None

            print("page:", num_page, "to:", final_page)
            var_ports = page.xpath('/html/head/script[not(@src)]/text()')
            tr_table = page.xpath('//table[@id="proxylist"]/tr')
            if var_ports:
                var_ports_values = {}
                list_var_port = var_ports[0].strip().split(";")
                list_var_port.pop()
                for var in list_var_port:
                    var = var.split("=")
                    var_ports_values[var[0]] = var[1]

                for tr in tr_table:
                    text_ip = tr.xpath("td[2]/text()")[0]
                    encryt_port = tr.xpath("td[2]/script/text()")
                    if encryt_port:
                        encryt_port = self.get_between(encryt_port[0], 'document.write(":"', ")").replace("+", "")
                        for k, v in var_ports_values.items():
                            encryt_port = encryt_port.replace(k, v)

                        proxies.add("{}:{}".format(text_ip.strip(), encryt_port.strip()))
                        # [proxies.add("{}://{}:{}".format(h, text_ip.strip(), encryt_port.strip())) for h in ["http", "https"]]

                Data().insert_many(*proxies)
            if final_page and num_page >= final_page:
                print("End page")
                break

        print("end")


class ScrapeIpAdress(object):
    page_start = "http://www.ip-adress.com/proxy_list/?k=time&d=desc"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page = html.fromstring(content_page)
        tr_table = page.xpath('//table[@class="proxylist"]/tr')
        proxies = set()
        for i in range(2, len(tr_table) - 1):
            ip = tr_table[i].xpath('td[1]/text()')[0]

            proxies.add(ip)
            # [proxies.add("{}://{}".format(h, ip)) for h in ["http", "https"]]
        Data().insert_many(*proxies)


class ScrapeProxyDaily(object):
    page_start = "http://proxy-daily.com/"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = html.fromstring(content_page)
        pages_href = page_main.xpath('//a[@class="more-link"]/@href')
        for uri_page in pages_href:
            content_page = http.get(uri_page)
            if "You Are Banned." in content_page.decode():
                continue
            proxies = set()
            # re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add("http://{}".format(m.group(0))), content_page.decode())
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            Data().insert_many(*proxies)
            # break


class ScrapeProxyServerList(object):
    page_start = "http://proxyserverlist-24.blogspot.com.au"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = html.fromstring(content_page)
        pages_href = page_main.xpath('//div[@class="jump-link"]/a/@href')
        for uri_page in pages_href:
            content_page = http.get(uri_page)
            proxies = set()
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            # print(proxies)
            Data().insert_many(*proxies)
            # break


class ScrapeNewFreshProxies(object):
    page_start = "http://newfreshproxies-24.blogspot.com.au/"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = html.fromstring(content_page)
        pages_href = page_main.xpath('//div[@class="jump-link"]/a/@href')

        for uri_page in pages_href:
            content_page = http.get(uri_page)
            proxies = set()
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            # print(proxies)
            Data().insert_many(*proxies)
            # break


class ScrapeLiveSocksNet(object):
    page_start = "http://www.live-socks.net"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = html.fromstring(content_page)
        pages_href = page_main.xpath('//div[@class="jump-link"]/a/@href')

        for uri_page in pages_href:
            content_page = http.get(uri_page)
            proxies = set()
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            # print(proxies)
            Data().insert_many(*proxies)
            # break


class ScrapeVipSocks24(object):
    page_start = "http://www.vipsocks24.net/"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        page_main = html.fromstring(content_page)
        pages_href = page_main.xpath('//div[@class="jump-link"]/a/@href')

        for uri_page in pages_href:
            content_page = http.get(uri_page)
            proxies = set()
            re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
            # print(proxies)
            Data().insert_many(*proxies)
            # break


class ScrapeFreshProxiesOrg(object):
    page_start = "http://www.freshproxy.org/"

    def handle(self):
        uri = self.page_start
        content_page = http.get(uri)
        proxies = set()
        re.sub(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})[ \t:]+(\d{2,5})", lambda m: proxies.add(m.group(0)), content_page.decode())
        Data().insert_many(*proxies)


if __name__ == '__main__':
    klass = [ScrapeProxydb, ScrapeNntime, ScrapeIpAdress, ScrapeProxyDaily, ScrapeProxyServerList, ScrapeNewFreshProxies, ScrapeLiveSocksNet, ScrapeVipSocks24, ScrapeFreshProxiesOrg]
    # klass = [ScrapeIpAdress]
    for c in klass:
        try:
            print("page", c.page_start)
            if "Host" in req.headers:
                del req.headers["Host"]
            c().handle()
        except Exception as e:
            print(e)
            raise e
    # print("end app.")
    # ScrapeNntime().handle()
    # ScrapeIpAdress().handle()
    # ScrapeProxyDaily().handle()
    # ScrapeProxyServerList().handle()
    # ScrapeNewFreshProxies().handle()
    # ScrapeFreshProxiesOrg().handle()
    # ScrapeLiveSocksNet().handle()
    # ScrapeVipSocks24().handle()
# coding=utf8
# the above tag defines encoding for this document and is for Python 2.x compatibility

import re

regex = r"\D+"

test_str = ("sdsd 1212 sdsd")

subst = ""

# You can manually specify the number of replacements by changing the 4th argument
result = re.sub(regex, subst, test_str, 0)
print (regex)
print ("\n----------------------------------\n")
print (test_str)
print ("\n----------------------------------\n")
if result:
    print (result)

# Note: for Python 2.7 compatibility, use ur"" to prefix the regex and u"" to prefix the test string and substitution.

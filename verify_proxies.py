# -*- coding: utf-8 -*-
import asyncio
import aiohttp
import scrape_proxies


__author__ = '''
    user:     jad21 <Jose Angel>
    email:    esojangel@gmail.com
    whatsapp: +584120309959
'''

data = scrape_proxies.Data()
proxies_to_delete = []


async def get_semaphore(sem, proxy):
        async with sem:
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get("https://www.google.co.ve/", proxy=proxy) as resp:
                        body = await resp.text()
                        if resp.status == 200 and "google" in body:
                            print("response ok", proxy)
                        else:
                            proxies_to_delete.append(proxy)
                            print("delete", proxy)
            except Exception as e:
                print("Error in verify", proxy, e)
                proxies_to_delete.append(proxy)


async def main():
    proxies = data.all()
    print("Total", len(proxies))
    semaphore = asyncio.Semaphore(100)
    coro = []
    for proxy in proxies:
        coro.append(get_semaphore(semaphore, proxy))
    await asyncio.gather(*coro)
    print("to delete", len(proxies_to_delete), "proxy ips")
    data.delete(*proxies_to_delete)
    print("before", len(proxies), "current", len(data.all()))


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
